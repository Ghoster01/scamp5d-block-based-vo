#include <math.h>
#include <scamp5.hpp>

using namespace SCAMP5_PE;


#ifndef DREG_STORAGE_HG
#define DREG_STORAGE_HG

class DREG_STORAGE
{
public:
	static void STORE_3BIT_IMAGE();
	static void RETRIEVE_3BIT_IMAGE();

	static void STORE_4BIT_IMAGE();
	static void RETRIEVE_4BIT_IMAGE();
};

#endif
