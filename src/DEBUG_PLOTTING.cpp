/*
 * DEBUG_PLOTTING.cpp
 *
 *  Created on: 17 Mar 2018
 *      Author: agile
 */
#include <scamp5.hpp>
#include "DEBUG_PLOTTING.hpp"
using namespace SCAMP5_PE;

debug_graph::debug_graph(int data_length, double data_scaling, int update_interval)
{
	this->data_scaling = data_scaling;
	this->data_length = data_length;
	this->data = new uint8_t[data_length];
	this->update_iterval = update_iterval;
}

void debug_graph::update(double new_data)
{
	if(update_timer.get_usec() > update_iterval)
	{
		update_timer.reset();
		data[data_index] = ((int)(data_scaling*new_data)+128)%256;
		data_index = (data_index+1)%data_length;
	}
}

void debug_graph::draw_to_R11()
{
  scamp5_kernel_begin();
  	  CLR(R11);
  scamp5_kernel_end();
  scamp5_draw_begin(R11);
  int skip = 256/this->data_length;
  for(int n = 0 ; n < this->data_length -1 ; n++)
  {
	  if(n+1!= data_index)
	  {
		  scamp5_draw_line(n*skip,data[n],(n+1)*skip,data[n+1],true);
	  }
  }
  scamp5_draw_end();
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


debug_dotmat_graph::debug_dotmat_graph(int update_rate,double scaling1, double scaling2, double scaling3, double scaling4)
{
	this->set_rate(update_rate);
	this->values = new double[4];
	this->prev_plotted_values = new uint8_t[4];
	this->value_scaling = new double[4];

	this->value_scaling[0] = scaling1;
	this->value_scaling[1] = scaling2;
	this->value_scaling[2] = scaling3;
	this->value_scaling[4] = scaling4;
}

void debug_dotmat_graph::set_rate(int frame_rate)
{
	plotting_rate = 1000000/frame_rate;
}


void debug_dotmat_graph::update_value(uint8_t index, double value)
{
	if(plotting_timer.get_usec() > plotting_rate)
	{
		values[index] = value;
	}
}


void debug_dotmat_graph::draw_DOTMAT(vs_dotmat dotmat)
{
	//clear column to draw on
	dotmat.draw_line(0,plotting_index,255,plotting_index,true,0);

	//draw dotted middle line
	dotmat.draw_line(127,0,127,255,true);

	if(plotting_index > 0)
	{
		for(int n = 0 ; n < plots ; n++)
		{
			int plotted_value = ((int)(value_scaling[n]*values[n])+128)%256;
			if(abs(plotted_value-prev_plotted_values[n])< 128)
			{
				dotmat.draw_line(plotted_value,plotting_index,prev_plotted_values[n],plotting_index-1,false,true);
			}
			prev_plotted_values[n] = plotted_value;
		}
	}
	plotting_index = (plotting_index+1)%256;
	plotting_timer.reset();
}

void debug_dotmat_graph::draw_4_windows_DOTMAT(vs_dotmat dotmat)
{
	if(plotting_index > 0)
	{
		for(int n = 0 ; n < plots ; n++)
		{
			int xoff = ((n+1)%2)*128;
			int yoff = n > 1 ? 128 : 0;

			dotmat.draw_line(yoff,plotting_index+xoff,yoff+128,plotting_index+xoff,true,0);

			int plotted_value = ((int)(value_scaling[n]*0.5*values[n])+64)%128;
			plotted_value += plotted_value < 0 ? 128 : 0;
			if(abs(plotted_value-prev_plotted_values[n])< 64)
			{
				dotmat.draw_line(plotted_value+yoff,plotting_index+xoff,prev_plotted_values[n]+yoff,plotting_index-1+xoff,false,true);
			}
			prev_plotted_values[n] = plotted_value;
		}
	}
	plotting_index = (plotting_index+1)%128;
	plotting_timer.reset();

	//draw graph lines
	dotmat.draw_line(127,0,127,255,true);
	dotmat.draw_line(0,127,255,127,true);
}

