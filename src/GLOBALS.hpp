/*
 * GLOBALS.hpp
 *
 *  Created on: 13 Mar 2018
 *      Author: agile
 */

#ifndef GLOBALS_HPP_
#define GLOBALS_HPP_
#include <scamp5.hpp>
#include "DEBUG_PLOTTING.hpp"

vs_handle display_1,display_2;

enum class DISPLAY_MODES {BLOCK_VECTORS,GRAPHS};
vs_stopwatch display_trigger_timer;
static uint32_t debug_display_trigger_time = 16667; //IE 1,000,000/(MONITOR FPS), 60HZ = 16667, 100HZ = 10,000

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

double relative_yaw,relative_pitch,relative_roll,relative_scale;
double prev_relative_yaw,prev_relative_pitch,prev_relative_roll,prev_relative_scale;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int horizontal_blocks = 4, verticle_blocks = 4;
int blocks = 4;
int search_iterations = 5;

const int max_horizontal_blocks = 8;
const int max_verticle_blocks = 8;

int block_size_x = 256 / 4;
int block_size_y = 256 / 4;

int block_x_shifts[max_horizontal_blocks*max_verticle_blocks];
int block_y_shifts[max_horizontal_blocks*max_verticle_blocks];

int block_previous_x_shifts[max_horizontal_blocks*max_verticle_blocks];
int block_previous_y_shifts[max_horizontal_blocks*max_verticle_blocks];

bool take_keyframe, force_take_keyframe;
int use_shift_priors;
int max_block_shift = 4;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

debug_dotmat_graph dotmat_grapher(50,1,1,1,1);

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

vs_stopwatch text_timer;
uint8_t text_interval = 1000000/1000;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void start_up()
{

}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


struct filtered_dimension
{
public:
	int log_length = 64;

	double filter_value = 0.1;

	double value = 0;
	double prev_value = 0;
	double rate = 0;

	int8_t *raw_change_log;
	double change_log_sum = 0;
	int change_log_index = 0;

	filtered_dimension(int filter_length, double filter_value)
	{
		this->log_length = filter_length;
		raw_change_log = new int8_t[filter_length];
		this->filter_value = filter_value;
	}

	void update(int8_t raw_change, bool filter)
	{
		value = raw_change;
		rate = value-prev_value;
	}

	void update2(int8_t raw_change, bool filter)
	{
		change_log_sum -= raw_change_log[change_log_index];
		change_log_sum += raw_change;
		raw_change_log[change_log_index] = raw_change;
		change_log_index = (change_log_index+1)%log_length;

		prev_value = value;
		double filtered_change = change_log_sum/log_length;
		if(fabs(filtered_change) < filter_value && filter)
		{
			filtered_change = 0;
		}

		value = value + filtered_change;
		rate = value-prev_value;
	}

	void reset()
	{
		value = 0;
		prev_value = 0;
		change_log_sum = 0;
		for(int n = 0 ; n < log_length ; n++)
		{
			raw_change_log[n] = 0;
		}
	}
};

filtered_dimension filtered_roll (64,0.1);
filtered_dimension filtered_scale(64,0.1);
filtered_dimension filtered_yaw (4,0.1);
filtered_dimension filtered_pitch (4,0.1);

debug_graph yaw_graph(128,0.5,2000000);
debug_graph pitch_graph(128,0.5,2000000);
debug_graph roll_graph(128,0.5,2000000);
debug_graph zoom_graph(128,0.5,2000000);

#endif /* GLOBALS_HPP_ */
