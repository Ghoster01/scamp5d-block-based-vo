#include <math.h>
#include <scamp5.hpp>
#include "Eigen/Dense"
#include <random>
#include "EIGEN_FUNCTIONS.hpp"
#include "DREG_IMAGE_STORAGE.hpp"
#include "GLOBALS.hpp"
#include "BLOCK_TRACKING.hpp"
#include "DEBUG_DRAWING.hpp"

using namespace SCAMP5_PE;

vs_dotmat dotmat;
static int debug_display = 0;


volatile int readout_mode;
volatile int threshold;

vs_stopwatch test_timer;
vs_stopwatch total_timer;

void refresh_dreg_storage()
{
	scamp5_kernel_begin();
		REFRESH(R5);REFRESH(R6);REFRESH(R7);REFRESH(R8);
	scamp5_kernel_end();
}


void draw_grid(int offset_x, int offset_y, int line_spacing, int line_width)
{
	scamp5_kernel_begin();
		CLR(R10);
	scamp5_kernel_end();

	scamp5_draw_begin(R10);

	int ylocation = offset_x%line_spacing;
	while(ylocation < 255)
	{
		scamp5_draw_line(0,ylocation,255,ylocation);
		ylocation+=line_spacing;
	}

	int xlocation = offset_y%line_spacing;
	while(xlocation < 255)
	{
		scamp5_draw_line(xlocation,0,xlocation,255);
		xlocation+=line_spacing;
	}

	for(int n = 0 ; n < line_width ; n++)
	{
		scamp5_kernel_begin();
			DNEWS(R11,R10,east | west | south | north);
			MOV(R1,R10);
			OR(R10,R1,R11);
		scamp5_kernel_end();
	}

	scamp5_draw_end();
}

void scamp5_main() {
	display_1 = vs_gui_add_display("Measurements", 0, 0, 2);
	display_2 = vs_gui_add_display("Image", 0, 2, 2);
//	display_3 = vs_gui_add_display("Image", 0, 1, 1);

	vs_gui_add_slider("display ", 0, 4, 0, &debug_display);

	vs_gui_add_slider("blocks (N X N): ", 2, max_verticle_blocks, 4, &blocks);
	vs_gui_add_slider("search_iterations: ", 1, 40, 5, &search_iterations);
	auto gui_button_force_keyframe = vs_gui_add_button("force take keyframe");
	vs_on_gui_update(gui_button_force_keyframe, [&](int32_t new_value) {
			force_take_keyframe = true;
		});

	vs_gui_add_switch("use shift priors",true,&use_shift_priors);
	vs_gui_add_slider("max block shift: ", 4, 32, 4, &max_block_shift);

	auto gui_button_view_keyframe = vs_gui_add_button("view keyframe");
	vs_on_gui_update(gui_button_view_keyframe, [&](int32_t new_value) {
			scamp5_output_image(B,display_2);
		});

	vs_on_gui_update(VS_GUI_FRAME_RATE, [&](int32_t new_value) {
		uint32_t framerate = new_value;
		if (framerate > 0) {
			vs_frame_trigger_set(1, framerate);
			vs_enable_frame_trigger();
//			vs_post_text("frame trigger: 1/%d\n", (int)framerate);
		}
		else {
			vs_disable_frame_trigger();
//			vs_post_text("frame trigger disabled\n");
		}
	});

	vs_on_host_connect([&]() {
//		vs_post_text("Scamp5d Image Storage\n");
//		vs_post_text("loop_counter: %d\n", (int)vs_loop_counter_get());
		scamp5_kernel::print_debug_info();
		vs_led_on(VS_LED_2);
	});

	vs_on_host_disconnect([&]() {
		vs_led_off(VS_LED_2);
	});

	vs_frame_trigger_set(1, 50);

	Eigen::VectorXf measurement;
	Eigen::VectorXf estimates_OLS;
	Eigen::VectorXf estimates_ransac;

	measurement = Eigen::VectorXf::Zero(2 * horizontal_blocks*verticle_blocks, 1);
	estimates_OLS = Eigen::VectorXf::Zero(2 * horizontal_blocks*verticle_blocks, 1);
	estimates_ransac = Eigen::VectorXf::Zero(2 * horizontal_blocks*verticle_blocks, 1);

	start_up();

	total_timer.reset();

	//main loop
	while (1) {

		test_timer.reset();

		refresh_dreg_storage();

		if (blocks % 2 == 0)
		{
			horizontal_blocks = blocks;
			verticle_blocks = blocks;
		}
		else
		{
			horizontal_blocks = 4;
			verticle_blocks = 4;
		}

		// this function is where all the interaction with host happens
		vs_process_message();

		// frame trigger
		vs_wait_frame_trigger();

		//bind post channel to display 1 for showing dot mat
		vs_post_set_channel((uint32_t)display_1);



		if(true)
		{
			//get new image
			scamp5_get_image(A, E, vs_gui_read_slider(VS_GUI_FRAME_GAIN));
		}
		else
		{
			int spacing = 30;
			int width = 3;
			double speed = 10*0.003;
//			int offsetx = round(sin(vs_loop_counter_get()*speed)*50);
//			int offsety = round(cos(vs_loop_counter_get()*speed)*50);
//			draw_grid_basic(50+offsetx,50+offsety,spacing,width);
//
//			scamp5_in(A,-100);
//			scamp5_kernel_begin();
//				WHERE(R10);
//					scamp5_kernel_end();
//						scamp5_in(A,100);
//					scamp5_kernel_begin();
//				all();
//			scamp5_kernel_end();
//
//			double double_offset_ang = cos(vs_loop_counter_get()*speed)*0.5;
//
//			scamp5_full_scale_AREG(AREG::A,fabs(double_offset_ang*30), double_offset_ang > 0 ? 1 : 0);
//			scamp5_img_rot_via_3_skews(AREG::A,double_offset_ang,true);
//
//			scamp5_kernel_begin();
//				where(A);
//					MOV(R10,FLAG);
//				all();
//			scamp5_kernel_end();


//
//			double segement_length = 500;
//			double segment_duration = 200;
//			int duration = segment_duration*4;
//			double segment_progress = (int)(segement_length*sin(M_PI*0.5*(vs_loop_counter_get()%(int)segment_duration)/segment_duration));
////			segment_progress = (int)(segement_length*(vs_loop_counter_get()%(int)segment_duration)/segment_duration);
//			if(vs_loop_counter_get()%duration < segment_duration)
//			{
//				int offsetx = segment_progress;
//				int offsety = 0;
//				draw_grid_basic(offsetx,offsety,spacing,width);
//			}
//			else
//			{
//				if(vs_loop_counter_get()%duration < segment_duration*2)
//				{
//					int offsetx = segement_length;
//					int offsety = segment_progress;
//					draw_grid_basic(offsetx,offsety,spacing,width);
//				}
//				else
//				{
//					if(vs_loop_counter_get()%duration < segment_duration*3)
//					{
//						int offsetx = 500-segment_progress;
//						int offsety = 500;
//						draw_grid_basic(offsetx,offsety,spacing,width);
//					}
//					else
//					{
//						int offsetx = 0;
//						int offsety = 500-segment_progress;
//						draw_grid_basic(offsetx,offsety,spacing,width);
//					}
//				}
//			}
//
//			scamp5_in(A,-100);
//			scamp5_kernel_begin();
//				WHERE(R10);
//					scamp5_kernel_end();
//						scamp5_in(A,100);
//					scamp5_kernel_begin();
//				all();
//			scamp5_kernel_end();
//
//
//			double double_offset_ang = cos(vs_loop_counter_get()*speed*2)*0.001;
//			scamp5_img_rot_via_3_skews(AREG::A,double_offset_ang,true);
//
//			scamp5_kernel_begin();
//				where(A);
//					MOV(R10,FLAG);
//				all();
//			scamp5_kernel_end();
		}

//		scamp5_output_image(A,display_4);

		if(take_keyframe || force_take_keyframe)
		{
			scamp5_kernel_begin();
				mov(F,A);
			scamp5_kernel_end();
			DREG_STORAGE::STORE_4BIT_IMAGE();
			scamp5_kernel_begin();
				MOV(R5,R1);MOV(R6,R2);MOV(R7,R3);MOV(R8,R4);
			scamp5_kernel_end();

			relative_yaw = 0;
			relative_pitch = 0;
			relative_roll = 0;
			relative_scale = 0;
			prev_relative_yaw = 0;
			prev_relative_pitch = 0;
			prev_relative_roll = 0;
			prev_relative_scale = 0;

			if(force_take_keyframe)
			{
				filtered_yaw.reset();
				filtered_pitch.reset();
				filtered_roll.reset();
				filtered_scale.reset();
			}

			take_keyframe = false;
			force_take_keyframe = false;

//			scamp5_output_image(A,display_4);

			for (int x = 0; x < horizontal_blocks; x++)
			{
				for (int y = 0; y < verticle_blocks; y++)
				{
					refresh_dreg_storage();

					int block_index = y * horizontal_blocks + x;

					block_previous_x_shifts[block_index] = 0;
					block_previous_y_shifts[block_index] = 0;

					block_x_shifts[block_index] = 0;
					block_y_shifts[block_index] = 0;
				}
			}
		}


		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


		scamp5_kernel_begin();
			MOV(R1,R5);MOV(R2,R6);MOV(R3,R7);MOV(R4,R8);
		scamp5_kernel_end();
		DREG_STORAGE::RETRIEVE_4BIT_IMAGE();
		scamp5_kernel_begin();
			mov(B,F);
		scamp5_kernel_end();


		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		block_tracking();

		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		refresh_dreg_storage();

		for (int x = 0; x < horizontal_blocks; x++)
		{
			for (int y = 0; y < verticle_blocks; y++)
			{
				refresh_dreg_storage();
				int block_index = y * horizontal_blocks + x;
				int id = block_index * 2;
				measurement(id) = block_x_shifts[block_index];
				measurement(id + 1) = block_y_shifts[block_index];

				if(!take_keyframe && (abs(block_x_shifts[block_index]) > max_block_shift ||
					abs(block_y_shifts[block_index]) > max_block_shift))
				{
					take_keyframe = true;
				}

				block_previous_x_shifts[block_index] = block_x_shifts[block_index];
				block_previous_y_shifts[block_index] = block_y_shifts[block_index];

				block_x_shifts[block_index] = 0;
				block_y_shifts[block_index] = 0;
			}
		}
		refresh_dreg_storage();



		// ---------------------  get model
		Eigen::MatrixXf H = generate_base_vectors(horizontal_blocks, verticle_blocks);

		// --------------------- get OLS estimates
		Eigen::Vector4f motion_OLS = estimate_OLS_motion(H, measurement);
		estimates_OLS = H * motion_OLS;

		prev_relative_roll = relative_roll;
		relative_roll = motion_OLS(0);

		prev_relative_scale = relative_scale;
		relative_scale = motion_OLS(1);

		prev_relative_yaw = relative_yaw;
		relative_yaw = motion_OLS(2);

		prev_relative_pitch = relative_pitch;
		relative_pitch = motion_OLS(3);

		filtered_yaw.update2(relative_yaw-prev_relative_yaw,false);
		filtered_pitch.update2(relative_pitch-prev_relative_pitch,false);
		filtered_roll.update2(relative_roll-prev_relative_roll,false);
		filtered_scale.update2(relative_scale-prev_relative_scale,false);

		yaw_graph.update(filtered_yaw.value);
		pitch_graph.update(filtered_pitch.value);
		roll_graph.update(filtered_roll.value);
		zoom_graph.update(filtered_scale.value);

		// --------------------- plot OLS estimates
		if((DISPLAY_MODES)debug_display == DISPLAY_MODES::BLOCK_VECTORS)
		{
			dotmat.clear();
			for (int x = 0; x < horizontal_blocks; x++)
			{
				for (int y = 0; y < verticle_blocks; y++)
				{
					refresh_dreg_storage();

					int block_index = y * horizontal_blocks + x;
					int posx1 = block_size_x * (x + 0.5);
					int posy1 = block_size_y * (y + 0.5);

					int id = block_index * 2;
					int posx2 = posx1 + estimates_OLS(id) * 2;
					int posy2 = posy1 + estimates_OLS(id + 1) * 2;

					dotmat.draw_line(posy1, posx1, posy2, posx2, true);

//					posx2 = posx1 + block_x_shifts[block_index] * 2;
//					posy2 = posy1 + block_y_shifts[block_index] * 2;
//
//					dotmat.draw_line(posy1, posx1, posy2, posx2, true);
				}
			}
		}

		update_display(display_1,debug_display);

		if(text_timer.get_usec() > text_interval)
		{
			text_timer.reset();
			vs_post_text("%.2f, %.2f, %.2f, %.2f, %d \n",filtered_yaw.value,filtered_pitch.value,filtered_roll.value,filtered_scale.value,total_timer.get_usec());
		}


//			scamp5_output_image(R11, display_3);
//		vs_post_text("time us %d fps %f \n",test_timer.get_usec(),1000000.0/test_timer.get_usec());

		refresh_dreg_storage();

		//std::free(ptr);
		if (vs_loop_counter_get() % 25 == 0) {
			vs_led_toggle(VS_LED_1);
		}
		vs_loop_counter_inc();
	}

}

int main(){

    // initialise M0 system
    vs_init();

    // make default output to be USB
    vs_post_bind_io_agent(vs_usb);

    scamp5_bind_io_agent(vs_usb);

    vs_on_shutdown([&](){
        vs_post_text("M0 shutdown\n");
    });

    auto bw_image_buffer = vs_prm->malloc(vs_dotmat::get_buffer_size(256,256));// vs_prm is used to allocate memory in a 64 KB memory block that is not managed by M4/M0
	dotmat.bind_buffer(bw_image_buffer,256,256);


    // run the vision algorithm

    scamp5_main();

    return 0;
}
