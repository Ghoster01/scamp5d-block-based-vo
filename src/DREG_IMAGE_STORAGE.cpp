#include <math.h>
#include <scamp5.hpp>
#include "DREG_IMAGE_STORAGE.hpp"

using namespace SCAMP5_PE;

void DREG_STORAGE::STORE_3BIT_IMAGE()
{
	//STORES F INTO R1 R2 R3
	const int value = 256/8;

	scamp5_kernel_begin();
		CLR(R1,R2,R3);
	scamp5_kernel_end();

	scamp5_in(E,127);
	scamp5_kernel_begin();
		mov(C,F);
	scamp5_kernel_end();

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	//BIT 3
	scamp5_kernel_begin();
		where(C);
			MOV(R3,FLAG);
		all();
	scamp5_kernel_end();

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	//BIT 2
	scamp5_in(D,value*4-1);
	scamp5_kernel_begin();
		add(F,C,E);
		WHERE(R3);
			sub(F,F,D);
		all();
	scamp5_kernel_end();

	scamp5_in(D,value*2-1);
	scamp5_kernel_begin();
		sub(F,F,D);
		where(F);
			MOV(R2,FLAG);
		all();
	scamp5_kernel_end();


	///////////////////////////////////////////////////////////////////////////////////////////////////////
	//BIT 1
	scamp5_in(D,value*4-1);
	scamp5_kernel_begin();
		add(F,C,E);
		WHERE(R3);
			sub(F,F,D);
		all();
	scamp5_kernel_end();

	scamp5_in(D,value*2-1);
	scamp5_kernel_begin();
		WHERE(R2);
			sub(F,F,D);
		all();
	scamp5_kernel_end();

	scamp5_in(D,value-1);
	scamp5_kernel_begin();
		sub(F,F,D);

		where(F);
			MOV(R1,FLAG);
		all();
	scamp5_kernel_end();
}

void DREG_STORAGE::RETRIEVE_3BIT_IMAGE()
{
	//RETRIEVES R1,R2,R3 INTO F
	const int value = 256/8;

	scamp5_in(D,value*8-129);
	scamp5_kernel_begin();
		respix(F);
		AND(R11,R1,R2);
		AND(R11,R11,R3);
		WHERE(R11);
			mov(F,D);
		all();
	scamp5_kernel_end();

	scamp5_in(D,value*7-129);
	scamp5_kernel_begin();
		NOT(R11,R1);
		AND(R11,R11,R3);
		AND(R11,R11,R2);
		WHERE(R11);
			mov(F,D);
		all();
	scamp5_kernel_end();

	scamp5_in(D,value*6-129);
	scamp5_kernel_begin();
		NOT(R11,R2);
		AND(R11,R11,R3);
		AND(R11,R11,R1);
		WHERE(R11);
			mov(F,D);
		all();
	scamp5_kernel_end();

	scamp5_in(D,value*5-129);
	scamp5_kernel_begin();
		NOR(R11,R1,R2);;
		AND(R11,R11,R3);
		WHERE(R11);
			mov(F,D);
		all();
	scamp5_kernel_end();

	scamp5_in(D,value*4-129);
	scamp5_kernel_begin();
		NOT(R11,R3);
		AND(R11,R11,R2);
		AND(R11,R11,R1);
		WHERE(R11);
			mov(F,D);
		all();
	scamp5_kernel_end();

	scamp5_in(D,value*3-129);
	scamp5_kernel_begin();
		NOR(R11,R1,R3);
		AND(R11,R11,R2);
		WHERE(R11);
			mov(F,D);
		all();
	scamp5_kernel_end();

	scamp5_in(D,value*2-129);
	scamp5_kernel_begin();
		NOR(R11,R2,R3);
		AND(R11,R11,R1);
		WHERE(R11);
			mov(F,D);
		all();
	scamp5_kernel_end();
}





/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////





void DREG_STORAGE::STORE_4BIT_IMAGE()
{
	//STORES F INTO R1 R2 R3
	const int value = 256/16;

	scamp5_kernel_begin();
		CLR(R1,R2,R3,R4);
	scamp5_kernel_end();

	scamp5_in(E,127);
	scamp5_kernel_begin();
		mov(C,F);
	scamp5_kernel_end();

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	//BIT 4
	scamp5_kernel_begin();
		where(C);
			MOV(R4,FLAG);
		all();
	scamp5_kernel_end();

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	//BIT 3
	scamp5_in(D,value*8-1);
	scamp5_kernel_begin();
		add(F,C,E);
		WHERE(R4);
			sub(F,F,D);
		all();
	scamp5_kernel_end();

	scamp5_in(D,value*4-1);
	scamp5_kernel_begin();
		sub(F,F,D);
		where(F);
			MOV(R3,FLAG);
		all();
	scamp5_kernel_end();


	///////////////////////////////////////////////////////////////////////////////////////////////////////
	//BIT 2
	scamp5_in(D,value*8-1);
	scamp5_kernel_begin();
		add(F,C,E);
		WHERE(R4);
			sub(F,F,D);
		all();
	scamp5_kernel_end();

	scamp5_in(D,value*4-1);
	scamp5_kernel_begin();
		WHERE(R3);
			sub(F,F,D);
		all();
	scamp5_kernel_end();

	scamp5_in(D,value*2-1);
	scamp5_kernel_begin();
		sub(F,F,D);

		where(F);
			MOV(R2,FLAG);
		all();
	scamp5_kernel_end();

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	//BIT 1
	scamp5_in(D,value*8-1);
	scamp5_kernel_begin();
		add(F,C,E);
		WHERE(R4);
			sub(F,F,D);
		all();
	scamp5_kernel_end();

	scamp5_in(D,value*4-1);
	scamp5_kernel_begin();
		WHERE(R3);
			sub(F,F,D);
		all();
	scamp5_kernel_end();

	scamp5_in(D,value*2-1);
	scamp5_kernel_begin();
		WHERE(R2);
			sub(F,F,D);
		all();
	scamp5_kernel_end();

	scamp5_in(D,value-1);
	scamp5_kernel_begin();
		sub(F,F,D);

		where(F);
			MOV(R1,FLAG);
		all();
	scamp5_kernel_end();
}





void DREG_STORAGE::RETRIEVE_4BIT_IMAGE()
{
	//RETRIEVES R1,R2,R3,R4 INTO F
	const int value = 256/16;

	scamp5_kernel_begin();
		respix(F);
	scamp5_kernel_end();

	scamp5_in(D,value*15-129);
	scamp5_kernel_begin();
		NOT(R11,R1);
		AND(R11,R11,R2);
		AND(R11,R11,R3);
		AND(R11,R11,R4);
		WHERE(R11);
			mov(F,D);
		all();
	scamp5_kernel_end();

	scamp5_in(D,value*14-129);
	scamp5_kernel_begin();
		NOT(R11,R2);
		AND(R11,R11,R1);
		AND(R11,R11,R3);
		AND(R11,R11,R4);
		WHERE(R11);
			mov(F,D);
		all();
	scamp5_kernel_end();

	scamp5_in(D,value*13-129);
	scamp5_kernel_begin();
		NOR(R11,R1,R2);
		AND(R11,R11,R3);
		AND(R11,R11,R4);
		WHERE(R11);
			mov(F,D);
		all();
	scamp5_kernel_end();

	scamp5_in(D,value*12-129);
	scamp5_kernel_begin();
		NOT(R11,R3);
		AND(R11,R11,R1);
		AND(R11,R11,R2);
		AND(R11,R11,R4);
		WHERE(R11);
			mov(F,D);
		all();
	scamp5_kernel_end();

	scamp5_in(D,value*11-129);
	scamp5_kernel_begin();
		NOR(R11,R1,R3);
		AND(R11,R11,R2);
		AND(R11,R11,R4);
		WHERE(R11);
			mov(F,D);
		all();
	scamp5_kernel_end();

	scamp5_in(D,value*10-129);
	scamp5_kernel_begin();
		NOR(R11,R2,R3);
		AND(R11,R11,R1);
		AND(R11,R11,R4);
		WHERE(R11);
			mov(F,D);
		all();
	scamp5_kernel_end();

	scamp5_in(D,value*9-129);
	scamp5_kernel_begin();
		NOR(R11,R1,R2,R3);
		AND(R11,R11,R4);
		WHERE(R11);
			mov(F,D);
		all();
	scamp5_kernel_end();

	scamp5_in(D,value*8-129);
	scamp5_kernel_begin();
		NOT(R11,R4);
		AND(R11,R11,R1);
		AND(R11,R11,R2);
		AND(R11,R11,R3);
		WHERE(R11);
			mov(F,D);
		all();
	scamp5_kernel_end();

	scamp5_in(D,value*7-129);
	scamp5_kernel_begin();
		NOR(R11,R1,R4);
		AND(R11,R11,R2);
		AND(R11,R11,R3);
		WHERE(R11);
			mov(F,D);
		all();
	scamp5_kernel_end();

	scamp5_in(D,value*6-129);
	scamp5_kernel_begin();
		NOR(R11,R2,R4);
		AND(R11,R11,R1);
		AND(R11,R11,R3);
		WHERE(R11);
			mov(F,D);
		all();
	scamp5_kernel_end();

	scamp5_in(D,value*5-129);
	scamp5_kernel_begin();
		NOR(R11,R1,R2,R4);
		AND(R11,R11,R3);
		WHERE(R11);
			mov(F,D);
		all();
	scamp5_kernel_end();

	scamp5_in(D,value*4-129);
	scamp5_kernel_begin();
		NOR(R11,R3,R4);
		AND(R11,R11,R1);
		AND(R11,R11,R2);
		WHERE(R11);
			mov(F,D);
		all();
	scamp5_kernel_end();

	scamp5_in(D,value*3-129);
	scamp5_kernel_begin();
		NOR(R11,R1,R3,R4);
		AND(R11,R11,R2);
		WHERE(R11);
			mov(F,D);
		all();
	scamp5_kernel_end();

	scamp5_in(D,value*2-129);
	scamp5_kernel_begin();
		NOR(R11,R2,R3,R4);
		AND(R11,R11,R1);
		WHERE(R11);
			mov(F,D);
		all();
	scamp5_kernel_end();

	scamp5_in(D,127);
	scamp5_kernel_begin();
		AND(R11,R1,R2);
		AND(R11,R11,R3);
		AND(R11,R11,R4);
		WHERE(R11);
			mov(F,D);
		all();
	scamp5_kernel_end();
}
