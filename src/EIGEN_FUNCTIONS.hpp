/*
 * EIGEN_FUNCTIONS.hpp
 *
 *  Created on: 13 Mar 2018
 *      Author: agile
 */
#include <math.h>
#include <scamp5.hpp>
#include "Eigen/Dense"
#include <random>

#ifndef EIGEN_FUNCTIONS_HPP_
#define EIGEN_FUNCTIONS_HPP_




std::vector<unsigned int> get_random(unsigned int min, unsigned int max, unsigned int howmany)
{
	std::vector<unsigned int> random_numbers;

	std::vector<unsigned int> a;
	std::vector<unsigned int>::iterator it;

	// Our Random Generator
	std::mt19937 eng{ std::random_device{}() };

	for (int iPoint = 1; iPoint <= howmany; iPoint++)
	{
		unsigned int q = std::uniform_int_distribution<unsigned int>{ min, max }(eng);

		do {
			q = std::uniform_int_distribution<unsigned int>{ min, max }(eng);
			it = std::find(a.begin(), a.end(), q);
		} while (it != a.end());

		a.push_back(q);

		//std::cout << q << std::endl;
		random_numbers.push_back(q);
	}

	return random_numbers;
}


Eigen::MatrixXf  generate_base_vectors(int horizontal_blocks, int verticle_blocks)
{
	// implementaion needs to inlcude all block sizes. It only supports 4x4 blocks

	/*
	Eigen::MatrixXf matrixB(4, 2 * horizontal_blocks*verticle_blocks);


	matrixB << 3, -3, 3, -1, 3, 1, 3, 3, 1, -3, 1, -1, 1, 1, 1, 3, -1, -3, -1, -1, -1, 1, -1, 3, -3, -3, -3, -1, -3, 1, -3, 3,
	-3, -3, -1, -3, 1, -3, 3, -3, -3, -1, -1, -1, 1, -1, 3, -1, -3, 1, -1, 1, 1, 1, 3, 1, -3, 3, -1, 3, 1, 3, 3, 3,
	1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0,
	0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1;

	// roll
	// z (zoom)
	// yaw
	// pitch

	//matrixB << -3, 3, -3, 1, -3, -1, -3, -3, -1, 3, -1, 1, -1, -1, -1, -3, 1, 3, 1, 1, 1, -1, 1, -3, 3, 3, 3, 1, 3, -1, 3, -3,
	//	       -3, -3, -1, -3, 1, -3, 3, -3, -3, -1, -1, -1, 1, -1, 3, -1, -3, 1, -1, 1, 1, 1, 3, 1, -3, 3, -1, 3, 1, 3, 3, 3,
	//	        1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0,
	//	        0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1;
	//

	std::cout << matrixB << std::endl;

	matrixB.row(0) = matrixB.row(0) / matrixB.row(0).norm();
	matrixB.row(1) = matrixB.row(1) / matrixB.row(1).norm();
	matrixB.row(2) = matrixB.row(2) / matrixB.row(2).norm();
	matrixB.row(3) = matrixB.row(3) / matrixB.row(3).norm();

	Eigen::MatrixXf H = matrixB.transpose();
	*/
	//std::cout << H.transpose() * H << std::endl;
	// The solution is (H'H)^(-1) * H' * m, but (H'H)^(-1) = I, i.e. H is orthonoraml

	int computation_block_x = horizontal_blocks * 2;
	int computation_block_y = verticle_blocks * 2;

	int c_x = horizontal_blocks;
	int c_y = verticle_blocks;

	Eigen::VectorXf     z( 2 * horizontal_blocks*verticle_blocks, 1);
	Eigen::VectorXf  roll( 2 * horizontal_blocks*verticle_blocks, 1);
	Eigen::VectorXf pitch( 2 * horizontal_blocks*verticle_blocks, 1);
	Eigen::VectorXf   yaw( 2 * horizontal_blocks*verticle_blocks, 1);

	int i = 0;
	for (int idx = 0; idx < horizontal_blocks; idx++)
	{
		for (int idy = 0; idy < verticle_blocks; idy++)
		{
			int block_index = idy * horizontal_blocks + idx;
			int x = 2 * idx - (c_x - 1);
			int y = 2 * idy - (c_y - 1);

			i = 2 * block_index;
			z(i) = x;
			roll(i) = -y;
			pitch(i) = 0;
			yaw(i) = 1;

			i = i + 1;
			z(i) = y;
			roll(i) = x;
			pitch(i) = 1;
			yaw(i) = 0;
		}
	}

	Eigen::MatrixXf H1(2 * horizontal_blocks*verticle_blocks, 4);

	roll = roll / roll.norm();
	z = z / z.norm();
	yaw = yaw / yaw.norm();
	pitch = pitch / pitch.norm();

	H1.col(0) = roll;
	H1.col(1) = z;
	H1.col(2) = yaw;
	H1.col(3) = pitch;

	return H1;
}



Eigen::Vector4f estimate_ransac_motion(Eigen::MatrixXf H, Eigen::VectorXf measurement, int horizontal_blocks, int verticle_blocks, float inlier_threshold)
{
	//-------- RANSAC parameters
	//-------- these parameters need to be adjusted
	int nPoints = 5;             // number of points in each sample
	float probability = 0.99;    // probability
	unsigned int N = horizontal_blocks * verticle_blocks;
	int minInlier = 4;			 // these need to be adjusted
	int maxInteration = 200;
	float eps = (float)(minInlier) / (float)(N);
	int iItr = 0;

	if (minInlier == N)
		iItr = 1;
	else
		iItr = ceil(log(1 - probability) / log(1 - pow(eps, nPoints)));

	maxInteration = std::max(1, std::min(iItr, maxInteration));

	int iBestInliers = 0;
	//std::cout << "maxInteration: " << maxInteration << std::endl;

	//-------- RANSAC vectors and containters
	Eigen::Vector4f best_motion_ransac;
	best_motion_ransac << 0.0, 0.0, 0.0, 0.0;

	Eigen::Vector4f motion_ransac;
	motion_ransac << 0.0, 0.0, 0.0, 0.0;

	Eigen::VectorXf error_ransac;
	error_ransac = Eigen::VectorXf::Zero(2 * horizontal_blocks*verticle_blocks, 1);

	//-------- looping for RANSAC
	for (int iRansacIteration = 0; iRansacIteration < maxInteration; iRansacIteration++)
	{
		// generate nPoints random number between [0 N-1]
		std::vector<unsigned int> random_numbers = get_random(0, N - 1, nPoints);
		//motion_ransac = calulate_ransac_motion(H, random_numbers);

		// get the random points from the model
		Eigen::MatrixXf subH = Eigen::MatrixXf::Zero(2 * nPoints, 4);
		Eigen::VectorXf subM = Eigen::VectorXf::Zero(2 * nPoints, 1);

		int iSubHindex = 0;
		for (std::vector<unsigned int>::iterator it = random_numbers.begin(); it != random_numbers.end(); ++it)
		{
			//std::cout << *(it) << std::endl;
			unsigned int i = *(it);
			int block_index = i;
			subH.row(iSubHindex) = H.row(2 * i);
			subM(iSubHindex) = measurement(2 * i);
			//subM(iSubHindex) = block_x_shifts[i];

			iSubHindex = iSubHindex + 1;
			subH.row(iSubHindex) = H.row(2 * i + 1);
			subM(iSubHindex)     = measurement(2 * i +1 );
			//subM(iSubHindex) = block_y_shifts[i];

			iSubHindex = iSubHindex + 1;
		}

		//solve for the motion (H'H)^(-1) * H' * m
		motion_ransac = ((subH.transpose() * subH).inverse()) * subH.transpose() * subM;

		/*std::cout << " alfa-ransac: " << motion_ransac(0)
		<< " beta-ransac: " << motion_ransac(1)
		<< " gama-ransac: " << motion_ransac(2)
		<< " delta-ransac: " << motion_ransac(3) << std::endl;*/


		// estimates_ransac = H * motion_ransac;
		error_ransac = measurement - H * motion_ransac;

		std::vector<float> vError;
		std::vector<bool>  vInliers;


		int iNumberofInliers = 0;

		for (int i = 0; i < N; i++)
		{
			float err = sqrt(error_ransac(2 * i)*error_ransac(2 * i) + error_ransac(2 * i + 1)*error_ransac(2 * i + 1));
			vError.push_back(err);
			//std::cout << err << std::endl;

			if (err < inlier_threshold)
			{
				vInliers.push_back(true);
				iNumberofInliers = iNumberofInliers + 1;
			}
			else
				vInliers.push_back(false);

		}

		//std::cout << "# inliers: " << iNumberofInliers << std::endl;

		if (iNumberofInliers > iBestInliers)
		{
			iBestInliers = iNumberofInliers;
			best_motion_ransac = motion_ransac;
		}
	}

	return best_motion_ransac;
}

Eigen::Vector4f estimate_OLS_motion(Eigen::MatrixXf H, Eigen::VectorXf measurement)
{
	Eigen::Vector4f motion;
	motion << 0.0, 0.0, 0.0, 0.0;

	motion = H.transpose() * measurement;
	/*std::cout << " alfa: " << motion(0)
	<< " beta: " << motion(1)
	<< " gama: " << motion(2)
	<< " delta: " << motion(3) << std::endl;*/

	return motion;
}


#endif /* EIGEN_FUNCTIONS_HPP_ */
