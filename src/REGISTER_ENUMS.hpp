/*
 * REGISTER_ENUMS.hpp
 *
 *  Created on: 1 Feb 2018
 *      Author: agile
 */

#ifndef REGISTER_ENUMS_HPP_
#define REGISTER_ENUMS_HPP_

enum class AREG {A,B,C,D,E,F};

enum class DREG {R0,R1,R2,R3,R4,R5,R6,R7,R8,R9,R10,R11,R12};

#endif /* REGISTER_ENUMS_HPP_ */
