/*
 * DEBUG_PLOTTING.hpp
 *
 *  Created on: 17 Mar 2018
 *      Author: agile
 */

#ifndef DEBUG_PLOTTING_HPP_
#define DEBUG_PLOTTING_HPP_
struct debug_graph
{
public:
	uint8_t *data;
	uint8_t data_length;
	int data_index = 0;
	double data_scaling = 1;

	vs_stopwatch update_timer;
	uint32_t update_iterval;

	debug_graph(int data_length, double data_scaling, int update_interval);

	void update(double new_data);

	void draw_to_R11();
};

struct debug_dotmat_graph
{
public:
	uint8_t plotting_index;
	int plotting_rate;
	vs_stopwatch plotting_timer;
	const int plots = 4;

	double *values;
	uint8_t *prev_plotted_values;
	double *value_scaling;

	debug_dotmat_graph(int update_rate,double scaling1, double scaling2, double scaling3, double scaling4);

	void set_rate(int frame_rate);

	void update_value(uint8_t index, double value);

	void draw_DOTMAT(vs_dotmat dotmat);

	void draw_4_windows_DOTMAT(vs_dotmat dotmat);
};
#endif /* DEBUG_PLOTTING_HPP_ */
