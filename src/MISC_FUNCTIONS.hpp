#include <scamp5.hpp>
#include <math.h>
#include "REGISTER_ENUMS.hpp"

using namespace SCAMP5_PE;

#ifndef MISC_FUNCTIONS
#define MISC_FUNCTIONS
	unsigned char reverse_byte(unsigned char x);
	double sin_approx3(double angle);
	double cos_approx3(double angle);
	double acos_approx3(double value);
	double tan_approx3(double angle);
#endif
