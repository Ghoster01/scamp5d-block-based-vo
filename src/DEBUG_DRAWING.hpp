#include <scamp5.hpp>
#include "GLOBALS.hpp"

using namespace SCAMP5_PE;

extern uint8_t GD_STR_log[];
extern uint8_t GD_log_index;

extern vs_dotmat dotmat;

/////////////

void update_display(vs_handle display,int display_mode)
{
	//ONLY SEND BACK DEBUG IMAGES AT CERTAIN FPS
//	scamp5_output_image(A,display_4);
	if(display_trigger_timer.get_usec() > debug_display_trigger_time)
	{

		display_trigger_timer.reset();

		switch ((DISPLAY_MODES)display_mode)
		{
			case DISPLAY_MODES::BLOCK_VECTORS:
				vs_post_dotmat(dotmat);
				break;

			case DISPLAY_MODES::GRAPHS:
				dotmat_grapher.update_value(0,filtered_yaw.value);
				dotmat_grapher.update_value(1,filtered_pitch.value);
				dotmat_grapher.update_value(2,filtered_roll.value);
				dotmat_grapher.update_value(3,filtered_scale.value);
				dotmat_grapher.draw_4_windows_DOTMAT(dotmat);
				vs_post_dotmat(dotmat);
				break;
		}
	}
}

