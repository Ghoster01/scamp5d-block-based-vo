#include <math.h>
#include <scamp5.hpp>
#include "GLOBALS.hpp"

using namespace SCAMP5_PE;

#ifndef BLOCK_TRACKING_HPP_
#define BLOCK_TRACKING_HPP_

enum class SHIFT_DIRECTIONS { NONE, NORTH, SOUTH, EAST, WEST };


#define GLOBAL_SUM_SAMPLES 16
uint8_t sum_results[GLOBAL_SUM_SAMPLES];

int evaluate_shift_direction()
{
	scamp5_kernel_begin();
	//create absolute difference image between current shifted image and previous frame
	sub(F, E, B);
	abs(E, F);

	//apply block mask stored in R5
	WHERE(R11);
	mov(E, D);
	all();
	scamp5_kernel_end();

	//perform global sum
#if GLOBAL_SUM_SAMPLES == 16
	scamp5_global_sum_4x4(E, sum_results);
#endif
#if GLOBAL_SUM_SAMPLES == 64
	scamp5_global_sum_8x8(E, sum_results);
#endif

	//calculate global sum total from samples
	int total_sum_value = 0;
	for (int n = 0; n < GLOBAL_SUM_SAMPLES; n++)
	{
		total_sum_value = total_sum_value + sum_results[n];
	}

	return total_sum_value;
}

void block_tracking()
{
	int block_size_x = 256 / horizontal_blocks;
	int block_size_y = 256 / verticle_blocks;
	for (int x = 0; x < horizontal_blocks; x++)
	{
		for (int y = 0; y < verticle_blocks; y++)
		{
			int block_index = y * horizontal_blocks + x;

			int min_sum_value = 0;

			int WEST_shift_score = 0;
			int EAST_shift_score = 0;
			int SOUTH_shift_score = 0;
			int NORTH_shift_score = 0;

			//create mask for current block
			scamp5_load_rect(R1, y*block_size_y, x*block_size_x, (y + 1)*block_size_y - 1, (x + 1)*block_size_x - 1);
			//scamp5_load_rect(R1, 3*64, 0, 4*64-1, 64);
			scamp5_kernel_begin();
			NOT(R11, R1);

			//copy current image
			mov(C, A);
			scamp5_kernel_end();

			if(use_shift_priors)
			{
				scamp5_shift(C,block_previous_x_shifts[block_index],-block_previous_y_shifts[block_index]);
				block_x_shifts[block_index] = block_previous_x_shifts[block_index];
				block_y_shifts[block_index] = block_previous_y_shifts[block_index];
			}

			//fill D with mask value
			scamp5_in(D, 0);

			//////////////////////////////////////////////////////////////////////////////////////
			//EVALUTE NO SHIFT
			scamp5_kernel_begin();
			mov(E, C);
			scamp5_kernel_end();
			min_sum_value = evaluate_shift_direction();
			SHIFT_DIRECTIONS chosen_action = SHIFT_DIRECTIONS::NONE;

			for (int n = 0; n < search_iterations; n++)
			{
				//reset chosen action
				chosen_action = SHIFT_DIRECTIONS::NONE;

				//////////////////////////////////////////////////////////////////////////////////////
				//EVALUTE EAST SHIFT
				scamp5_kernel_begin();
				mov(E, C, east);
				scamp5_kernel_end();

				EAST_shift_score = evaluate_shift_direction();
				if (min_sum_value > EAST_shift_score)
				{
					min_sum_value = EAST_shift_score;
					chosen_action = SHIFT_DIRECTIONS::EAST;
				}

				//////////////////////////////////////////////////////////////////////////////////////
				//EVALUTE EAST SHIFT
				scamp5_kernel_begin();
				mov(E, C, west);
				scamp5_kernel_end();

				WEST_shift_score = evaluate_shift_direction();
				if (min_sum_value > WEST_shift_score)
				{
					min_sum_value = WEST_shift_score;
					chosen_action = SHIFT_DIRECTIONS::WEST;
				}

				//////////////////////////////////////////////////////////////////////////////////////
				//EVALUTE NORTH SHIFT
				scamp5_kernel_begin();
				mov(E, C, north);
				scamp5_kernel_end();

				NORTH_shift_score = evaluate_shift_direction();
				if (min_sum_value > NORTH_shift_score)
				{
					min_sum_value = NORTH_shift_score;
					chosen_action = SHIFT_DIRECTIONS::NORTH;
				}

				//////////////////////////////////////////////////////////////////////////////////////
				//EVALUTE SOUTH SHIFT
				scamp5_kernel_begin();
				mov(E, C, south);
				scamp5_kernel_end();

				SOUTH_shift_score = evaluate_shift_direction();
				if (min_sum_value > SOUTH_shift_score)
				{
					min_sum_value = SOUTH_shift_score;
					chosen_action = SHIFT_DIRECTIONS::SOUTH;
				}

				//////////////////////////////////////////////////////////////////////////////////////
				//SELECT BEST SHIFT DIRECTION
				switch (chosen_action)
				{
				case SHIFT_DIRECTIONS::EAST:
					scamp5_kernel_begin();
					mov(C, C, east);
					scamp5_kernel_end();
					block_x_shifts[block_index] = block_x_shifts[block_index] - 1;
					break;

				case SHIFT_DIRECTIONS::WEST:
					scamp5_kernel_begin();
					mov(C, C, west);
					scamp5_kernel_end();
					block_x_shifts[block_index] = block_x_shifts[block_index] + 1;
					break;

				case SHIFT_DIRECTIONS::NORTH:
					scamp5_kernel_begin();
					mov(C, C, north);
					scamp5_kernel_end();
					block_y_shifts[block_index] = block_y_shifts[block_index] + 1;
					break;

				case SHIFT_DIRECTIONS::SOUTH:
					scamp5_kernel_begin();
					mov(C, C, south);
					scamp5_kernel_end();
					block_y_shifts[block_index] = block_y_shifts[block_index] - 1;
					break;
				}

				//EXIT IF NO BEST DIRECTION
				if (chosen_action == SHIFT_DIRECTIONS::NONE)
				{
					break;
				}
			}
		}
	}
}



#endif /* BLOCK_TRACKING_HPP_ */
